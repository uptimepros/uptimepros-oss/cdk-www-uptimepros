
# Welcome to your CDK Python project!

This is a blank project for Python development with CDK.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

This project is set up like a standard Python project.  The initialization
process also creates a virtualenv within this project, stored under the `.venv`
directory.  To create the virtualenv it assumes that there is a `python3`
(or `python` for Windows) executable in your path with access to the `venv`
package. If for any reason the automatic creation of the virtualenv fails,
you can create the virtualenv manually.

To manually create a virtualenv on MacOS and Linux:

```
$ python3 -m venv .venv
```

After the init process completes and the virtualenv is created, you can use the following
step to activate your virtualenv.

```
$ source .venv/bin/activate
```

If you are a Windows platform, you would activate the virtualenv like this:

```
% .venv\Scripts\activate.bat
```

Once the virtualenv is activated, you can install the required dependencies.

```
$ pip install -r requirements.txt
```

At this point you can now synthesize the CloudFormation template for this code.

```
$ cdk synth
```

To add additional dependencies, for example other CDK libraries, just add
them to your `setup.py` file and rerun the `pip install -r requirements.txt`
command.

## Useful commands

 * `cdk ls`          list all stacks in the app
 * `cdk synth`       emits the synthesized CloudFormation template
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk docs`        open CDK documentation

Enjoy!


# dev
// install pipenv
python3 -m pip install pipenv
// use pipenv to install project dependencies
python3 -m pipenv install
// launch pipenv shell
python3 -m pipenv shell


# todo's
search "TODO" (should be case sensitive) to find places to change function / workflows.
They should also be noted here:
- src_webapp/app/django_example/settings.py
  - database configuration
  - static root directory, often set to a web server dir like /var/www/html


# meta list of TODO's
- app doesn't use django entrypoint
- app doesn't use webserver container
- deployment isn't started yet

[//]: # (system deployments with poetry https://github.com/python-poetry/poetry/issues/234)
poetry config settings.virtualenvs.create false

[//]: # (OR)

[//]: # (poetry export -f requirements.txt --output requirements.txt)
[//]: # (pip install -r requirements.txthttps://github.com/python-poetry/poetry/issues/234)


# create image repositories and build the image locally to test cdk. Build and push latest in pipeline as well (should be in ci.yaml).
aws ecr get-login-password --profile uptime_pros --region us-east-2 | docker login --username AWS --password-stdin 784000790509.dkr.ecr.us-east-2.amazonaws.com

<!-- create in cdk: https://docs.aws.amazon.com/cdk/api/v2/python/aws_cdk.aws_ecr/Repository.html -->
aws ecr create-repository --repository-name cdk-django-template/app --profile uptime_pros --region us-east-2
aws ecr create-repository --repository-name cdk-django-template/web --profile uptime_pros --region us-east-2

docker image build --file src_webapp/app/app.Dockerfile --tag 784000790509.dkr.ecr.us-east-2.amazonaws.com/cdk-django-template/app src_webapp/app
docker image build --file src_webapp/web/web.Dockerfile --tag 784000790509.dkr.ecr.us-east-2.amazonaws.com/cdk-django-template/web src_webapp/web

docker image push 784000790509.dkr.ecr.us-east-2.amazonaws.com/cdk-django-template/app
docker image push 784000790509.dkr.ecr.us-east-2.amazonaws.com/cdk-django-template/web

cdk takes an environment-name variable passed to stacks ie. dev/staging/rando-bob/production

each stack is built with that parameter so it can be used in naming which is important for their interconnections helping support multiple deployments per account (developers) (TODO think about making this a decorator). ci will always use branch name with certain branches destined for certain deployment accounts (TODO support this), and this allows non-cicd deployments for testing which go to an account of $AWS_PROFILE or $AWS_ACCOUNT_NUMBER (TODO check if there is a official env var this was a guess at name/existence).

Deploy process will build an ssm parameter store of all environment settings (TODO Support Writing!; TODO Support Reading? Maybe just verification?)
let's do this in a script which takes environment_name (branch name or custom name) as a parameter. use username by default?

The idea is, put stuff in environment so that local development still works, copy it to ssm parameter store so live deployments are stateful and can be easier to troubleshoot, some of the environment variables will just be used for informational purposes and won't be read "at runtime" of the app. Many are used "at deployment time" to write nginx.conf or settings.py for example.

The environment variables used so far can be found in tasks.configmap. Local deployment uses AWS_PROFILE, ci uses AWS_ACCESS_KEY_ID and AWS_SECRET_KEY

# usage
usage:  invoke deploy --environment=dev

docker container run -d -e POSTGRES_PASSWORD=password -p 5432:5432 --name django_example_db postgres
docker container exec --user postgres django_example_db psql --command='CREATE DATABASE mydb;'


# PROFIT!
stack goes up in about 19 minutes
stack goes down in about 13 minutes

hosted zone is looked up based on dns name. can support cross account eventually - see the cdk readme docs for help there.

# CW: clean up readme

# TODO - first time running for an environment this error often shows up - handled with ci retry for now but a long term solution would be much better: failed: Error [ValidationError]: S3 error: Access Denied
# TODO bootstrap an ec2 image (or something else) to use as the build agent to save minutes? Might not be worth it.
# TODO - make a container to shorten the processing of ci (building poetry and stuff takes a long time)
# TODO 
# TODO Use specific versions of everything everywhere
# - from in docker
# - all django deps
# - review/document ami lookup for ecs instance
# TODO Generate Django Secret Key and store in secret store instead of param store. Also, we shouldn't keep it in the ini file as we print the environment variables. Something has to give here. https://coderwall.com/p/bkkjgw/oneliner-to-generate-a-django-secret-key
# TODO - Support cross account for DNS usage. No one wants to only deploy this to a single account.
# TODO - figure out a better way to support static files
# TODO - change from "account wide" to specific instances some way? Maybe using SDK? ENI TRUNKING: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/container-instance-eni.html
# TODO - re-implement config check, reverted to just loading file for ease
# TODO - lock down django allowed hosts to just web containers
# TODO - make a task out of gitcommit.sh

# after forking, run this to add this repo as an upstream repository
git remote add upstream git@gitlab.com:jdix/cdk-django-template.git
# after forking, run this to add this repo as an upstream repository
git remote add upstream git@gitlab.com:jdix/cdk-django-template.git
(regularly run 'git pull upstream main' to get latest updates)

# then add new submodule as seen in replace_submodules.sh (can be called with the submodule git url being used)
# then update git project cicd variables (be sure to comment/remove aws_profile in cicd config_file variable)
cicd vars include aws_access_key_id, aws_cdk_version (ie. 2.0.0-rc.8), aws_secret_access_key, config_file, ssh_deploy_key (all of these are upper case fyi)
