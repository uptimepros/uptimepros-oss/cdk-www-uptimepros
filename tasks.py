from invoke import task, call
import os
import configparser
import sys
import json

# TODO update template-config.ini from config.ini (or at least offer suggestions for anything missing in either direction)

# variables (use env var in cicd)
if os.environ.get("CONFIG_FILE") != None:
    CONFIG_FILE = os.environ.get("CONFIG_FILE")
else:
    CONFIG_FILE = "config.ini"

MANAGE_PY = "src_webapp/app/manage.py"

# REQUIRED_CONFIG_MAP = {
#     "cdk_core": [
#         # "AWS_PROFILE",
#         "AWS_DEFAULT_REGION",
#         "CDK_DEFAULT_ACCOUNT",
#         "CDK_DEFAULT_REGION"
#     ],
#     "cdk_options": [
#         "BASTION_KEY_NAME"
#     ],
#     "application_integrations": [
#         "DJANGO_SECRET_KEY",
#         "DJANGO_DEBUG",
#         "DJANGO_PROJECT_NAME",
#         "DOMAIN_NAME",
#         "DJANGO_PORT"
#     ],
#     "application_options": []
# }

DEPLOY_ALL_STACK_LIST = [
    "VariableStack",
    "DataStack",
    "DjangoStack"
]

DEPLOY_TO_NONPROD_STACK_LIST = [
    "BastionStack"
]
DEPLOY_TO_PROD_STACK_LIST = []


# helper functions
def create_blank_config_file():
    with open(CONFIG_FILE, 'w') as f:
        f.write('see config map in tasks.py for structure\n')
        # TODO - let's write the config map to help out

def load_config_to_environment(config_parser):
    if not os.path.exists(CONFIG_FILE):
        create_blank_config_file()
        print("missing config file: " + CONFIG_FILE)
    else:
        for section in config_parser.keys():
            for key in config_parser[section].keys():
                os.environ[key.upper()] = config_parser[section][key]

        # for map_head in CONFIG_MAP.keys():
        #     for map_var in CONFIG_MAP[map_head]:
        #         if not ( map_head in config_parser.keys() != None and config_parser[map_head].get(map_var) != None ):
        #             # TODO LOG INFO / LOG ERROR
        #             print("missing required section:variable in config file: " + map_head + ":" + map_var)
        #             continue
        #         elif os.environ.get(map_var) != None and os.environ.get(map_var) != config_parser[map_head][map_var]:
        #             print("error - variable " + map_var + " defined in both environment and config file but they're different!")
        #             sys.exit(1)
                # TODO should we do this? does it promote accidents? For now, let's force using the config value
                # elif os.environ.get(map_var) != None:
                #     print("using " + map_var + " from environment. None defined in config file.")
                #     sys.exit(1)
                # else:
                #     print(map_head + ":" + map_var + "=" + config_parser[map_head].get(map_var))
                #     os.environ[map_var] = config_parser[map_head][map_var]


# pre tasks
@task
def load_environment_from_config_file(c):
    """
    This function helps make sure the config file conforms to our expectations.
    Then loads all the variables into the environment for code down the road.
    """
    config = configparser.ConfigParser()
    config.read(CONFIG_FILE)
    load_config_to_environment(config)

@task
def load_environment_from_launch_file(c):
    configurations = json.load(open('.vscode/launch.json'))["configurations"]
    # TODO ^ Document this relation in readme
    for configuration in configurations:
        if configuration["name"] == "Django":
            # TODO ^ Document this relation in readme
            for env_var in configuration["env"].keys():
                os.environ[env_var] = configuration["env"][env_var]
            break

# run tasks

if os.environ.get("CI") == "true":
    manage_pre = load_environment_from_config_file
else:
    manage_pre = load_environment_from_launch_file

@task(pre=[manage_pre])
def managepy(c, *args, **kwargs):
    arg_string = ""
    for arg in args:
        arg_string = arg_string + " " + arg
    for kwarg in kwargs.keys():
        # handle switch cases:
        value = " " if str(kwargs[kwarg]) == "True" or str(kwargs[kwarg]) == "False" else (" " + str(kwargs[kwarg]))
        arg_string = arg_string + " --" + kwarg + value
    command = "python " + MANAGE_PY + " " + arg_string
    c.run("bash -c '" + command + "'")

@task(pre=[load_environment_from_config_file])
def run(c, *args):
    command = ""
    for arg in args:
        command = command + " " + arg
    c.run(command)

# standard tasks
@task
def execute_django_dockerfile_template(c):
    c.run("envsubst '${DJANGO_PROJECT_NAME} ${DJANGO_PORT}' < src_webapp/app/app-template.Dockerfile > src_webapp/app/app.Dockerfile")

@task(pre=[load_environment_from_config_file, execute_django_dockerfile_template, call(managepy, "collectstatic", "--no-input"), call(run, "mkdir", "-p", "src_webapp/web/static"), call(run, "cp", "-r", "src_webapp/app/static_root/*", "src_webapp/web/static/")])
def deploy(c, environment, *args, **kwargs):
    stacks_to_deploy= []
    if kwargs.get("stacks") == None:
        for stack in DEPLOY_ALL_STACK_LIST:
            stacks_to_deploy.append(stack)
        if environment == "main":
            for stack in DEPLOY_TO_PROD_STACK_LIST:
                stacks_to_deploy.append(stack)
            # if kwargs.get("bastion") == True:
            #     stacks_to_deploy.append("BastionStack")
        else:
            for stack in DEPLOY_TO_NONPROD_STACK_LIST:
                stacks_to_deploy.append(stack)
        stack_name_list_string = " ".join(stacks_to_deploy)
    else:
        stack_name_list_string = kwargs.get("stacks")
    print("Deploying Stacks: " + stack_name_list_string)
    c.run("cdk deploy --require-approval=never --context config_file=" + CONFIG_FILE + " --context deployment=" + environment + " " + stack_name_list_string)

@task(pre=[load_environment_from_config_file, execute_django_dockerfile_template])
def destroy(c, environment):
    c.run("cdk destroy --force --context config_file=" + CONFIG_FILE + " --context deployment=" + environment + " --all")


@task(pre=[load_environment_from_config_file, execute_django_dockerfile_template, call(managepy, "collectstatic", "--no-input"), call(run, "mkdir", "-p", "src_webapp/web/static"), call(run, "cp", "-r", "src_webapp/app/static_root/*", "src_webapp/web/static/")])
def synth(c, environment):
    c.run("cdk synthesize --context config_file=" + CONFIG_FILE + " --context deployment=" + environment + " --all")

@task(pre=[load_environment_from_config_file, execute_django_dockerfile_template])
def bootstrap(c, environment):
    # local_aws_profile = os.environ.get("AWS_PROFILE")
    # profile_was_set = ( local_aws_profile != None )
    # if profile_was_set:
    #     os.unsetenv("AWS_PROFILE")
    # this command fails if AWS_PROFILE is set in env
    c.run("aws ecs put-account-setting-default --name awsvpcTrunking --value enabled --region " + os.environ["AWS_DEFAULT_REGION"])
    # if profile_was_set:
    #     os.environ["AWS_PROFILE"] = local_aws_profile
    c.run("cdk bootstrap --context config_file=" + CONFIG_FILE + " --context deployment=" + environment + " aws://" + os.environ["CDK_DEFAULT_ACCOUNT"] + "/" + os.environ["AWS_DEFAULT_REGION"])

@task
def commit(c, *args, **kwargs):
    arg_string = ""
    for arg in args:
        arg_string = arg_string + " " + arg
    with c.cd("src_webapp/app"):
        c.run("git add .")
        c.run("git commit -m'" + arg_string + "'")
        c.run("docker container commit " + os.environ['DEVELOPMENT_CONTAINER_NAME'] + " " + os.environ['DEVELOPMENT_CONTAINER_NAME'] + ":`git rev-parse HEAD | cut -c 1-8`")
