#!/usr/bin/env python3
import os
import aws_cdk as cdk
from deploy_webapp.webapp import (
    VariableStack,
    DjangoStack,
    DatabaseStack
    #, InfrastructureStack
)

from deploy_webapp.bastion import BastionStack

# app:
#   git_repo: "https://gitlab.com/uptimepros_clients/collective_strategies/mip-power-tools"
#   git_branch: "main"
  # database_type: "postgres"
  # PRIMARY DATABASE REGION FIRST
  # regions:
  #   - us-east-2
  # aws_profile: "uptime_pros"
# environment:
#   test_var: ""

app = cdk.App()

# If you don't specify 'env', this stack will be environment-agnostic.
# Account/Region-dependent features and context lookups will not work,
# but a single synthesized template can be deployed anywhere.

# Uncomment the next line to specialize this stack for the AWS Account
# and Region that are implied by the current CLI configuration.

env=cdk.Environment(account=os.getenv('CDK_DEFAULT_ACCOUNT'), region=os.getenv('CDK_DEFAULT_REGION'))

deployment_envirionment = app.node.try_get_context(key="deployment")

if deployment_envirionment == "main" or deployment_envirionment == "master":
    deployment_envirionment = "prod"

stack_slug = os.environ["PROJECT_SLUG"]

variable_stack = VariableStack(app, "VariableStack",
    deployment_environment=deployment_envirionment,
    stack_name=(deployment_envirionment + "-variable-stack-" + stack_slug),
    env=env,
)

database_stack = DatabaseStack(app, "DataStack",
    deployment_environment=deployment_envirionment,
    stack_name=(deployment_envirionment + "-data-stack-" + stack_slug),
    env=env,
)

django_stack = DjangoStack(app, "DjangoStack",
    deployment_environment=deployment_envirionment,
    stack_name=(deployment_envirionment + "-django-stack-" + stack_slug),
    env=env,
    vpc=database_stack.vpc,
    rds_dbi=database_stack.rds_dbi,
    rds_dbi_sg=database_stack.rds_dbi_sg,
    # termination_prevention=(deployment_envirionment=="prod")
)

BastionStack(app, "BastionStack",
    deployment_environment=deployment_envirionment,
    stack_name=(deployment_envirionment + "-bastion-stack-" + stack_slug),
    env=env,
    vpc=database_stack.vpc,
    webapp_asg_sg=django_stack.webapp_asg_sg,
)

app.synth()
