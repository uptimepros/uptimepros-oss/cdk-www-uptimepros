import constructs
import aws_cdk as cdk
import configparser
import os
from aws_cdk import (
    RemovalPolicy,
    aws_ssm as _ssm,
    aws_secretsmanager as _secrets,
    aws_rds as _rds,
    aws_ec2 as _ec2,
    aws_ecs as _ecs,
    aws_ecs_patterns as _ecs_patterns,
    aws_ecr as _ecr,
    aws_ecr_assets as _ecr_assets,
    aws_autoscaling as _autoscaling,
)

class BastionStack(cdk.Stack):

    def __init__(self, scope: constructs.Construct, construct_id: str, vpc: _ec2.Vpc, webapp_asg_sg: _ec2.SecurityGroup, deployment_environment=None, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        vpc = vpc

        bastion_instance = _ec2.Instance(
            self, "bastionHost",
            vpc=vpc,
            allow_all_outbound=True,
            instance_type=_ec2.InstanceType("t2.micro"),
            machine_image=_ec2.MachineImage.latest_amazon_linux(),
            vpc_subnets=_ec2.SubnetSelection(subnet_type=_ec2.SubnetType.PUBLIC),
            key_name=os.environ["BASTION_KEY_NAME"]
        )

        bastion_instance.connections.allow_from_any_ipv4(
            _ec2.Port(
                string_representation="ssh in to bastion",
                protocol=_ec2.Protocol.TCP,
                from_port=22,
                to_port=22,
            )
        )

        _ec2.CfnSecurityGroupIngress(
            self, "WebAppAllowingSSHInFromBasttion",
            ip_protocol="TCP",
            from_port=22,
            to_port=22,
            group_id=webapp_asg_sg.security_group_id,
            source_security_group_id=bastion_instance.connections.security_groups[0].security_group_id
        )