echo 'recommend clean working tree, script performs "git add ." and "git commit" commands'
rm .gitmodules
# vim .git/config (remove the submodule piece)
git rm --cached src_webapp/app
rmdir src_webapp/app
git add .
git commit -m 'remove submodule'
git submodule add --name DjangoProject $1 src_webapp/app
